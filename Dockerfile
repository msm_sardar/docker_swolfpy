FROM continuumio/miniconda3

MAINTAINER MojtabaSardarmehni <msardar2@ncsu.edu>

ARG DEBIAN_FRONTEND=noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update && apt-get install -y --no-install-recommends apt-utils && \
	apt-get install -y sudo && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN sudo apt-get update -y
#RUN sudo apt-get install -y libgl1-mesa-glx libnss3 libpulse-mainloop-glib0 python-pyqt5.qtwebkit libasound2-dev

RUN sudo apt-get install -y libnss3
#RUN sudo apt-get install -y libxi-dev
RUN sudo apt-get install -y libpulse-mainloop-glib0
RUN sudo apt-get install -y python-pyqt5.qtwebkit
RUN sudo apt-get install -y libasound2-dev

#RUN sudo apt-get install -y python3.7-dev gcc

RUN conda update conda

ADD environment_dev.yml /tmp/environment.yml
RUN conda env create -f /tmp/environment.yml

